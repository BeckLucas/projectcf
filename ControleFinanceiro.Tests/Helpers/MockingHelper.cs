using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ControleFinanceiro.Tests.Helpers
{
    internal static class MockingHelper
    {
        private static BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        public static T CreateInstance<T>(params object[] args)
        {
            var typeToCreate = typeof(T);

            var parameterTypes = args.Select(arg => arg.GetType()).ToArray();

            var constructorInfoObj = typeToCreate.GetConstructor(bindingAttr, null, parameterTypes, null);

            return (T)constructorInfoObj.Invoke(args);
        }

        public static void SetPropertyValue(object target, string memberName, object newValue)
        {
            var prop = GetPropertyReference(target.GetType(), memberName);
            prop.SetValue(target, newValue, null);
        }

        private static PropertyInfo GetPropertyReference(Type targetType, string memberName)
        {
            var propInfo = targetType.GetProperty(memberName, bindingAttr);

            if (propInfo == null && targetType.BaseType != null)
            {
                return GetPropertyReference(targetType.BaseType, memberName);
            }
            return propInfo;
        }

        public static void SetFieldValue(object target, string fieldName, object newValue)
        {
            var field = GetFieldReference(target.GetType(), fieldName);
            field.SetValue(target, newValue);
        }

        private static FieldInfo GetFieldReference(Type targetType, string fieldName)
        {
            var field = targetType.GetField(fieldName, bindingAttr);

            if (field == null && targetType.BaseType != null)
            {
                return GetFieldReference(targetType.BaseType, fieldName);
            }
            return field;
        }

        public static string GetPropertyName<T>(Expression<Func<T>> property)
        {
            var lambdaExpression = (LambdaExpression)property;
            var memberExpression = lambdaExpression.Body as MemberExpression ?? ((UnaryExpression)lambdaExpression.Body).Operand as MemberExpression;
            return memberExpression.Member.Name;
        }
    }
}
using System;

namespace ControleFinanceiro.Dominio.Entities
{
    public class Movimento : BaseEntity
    {
        public int IdMovimento { get; set; }
        public OrigemMovimento OrigemMovimento { get; set; }
        public CategoriaMovimento CategoriaMovimento { get; set; }
        public Conta Conta { get; set; }
        public DateTime DataInclusao { get; set; }
        public decimal Valor { get; set; }
        public int QtdParcelas { get; set; }
        public DateTime? DataVencimento { get; set; }
    }
}
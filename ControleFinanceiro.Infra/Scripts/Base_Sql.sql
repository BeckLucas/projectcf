CREATE TABLE dbo.TipoConta
(
	IdTipoConta INT IDENTITY NOT NULL,
	Descricao VARCHAR(100) NOT NULL
)
GO
ALTER TABLE dbo.TipoConta ADD CONSTRAINT Pk_TipoConta PRIMARY KEY (IdTipoConta)
GO

CREATE TABLE dbo.Conta
(
	IdConta INT IDENTITY NOT NULL,
	TitularConta VARCHAR(100) NOT NULL,
	IdTipoConta INT NOT NULL
)
GO
ALTER TABLE dbo.Conta ADD CONSTRAINT Pk_Conta PRIMARY KEY (IdConta)
GO
ALTER TABLE dbo.Conta ADD CONSTRAINT FK_Conta_TipoConta FOREIGN KEY (IdTipoConta) REFERENCES dbo.TipoConta (IdTipoConta)
GO

CREATE TABLE dbo.CategoriaMovimento
(
	IdCategoriaMovimento INT IDENTITY NOT NULL,
	Descricao VARCHAR(100) NOT NULL,
	DataInclusao DATETIME NOT NULL DEFAULT GETDATE()
)
GO
ALTER TABLE dbo.CategoriaMovimento ADD CONSTRAINT Pk_CategoriaMovimento PRIMARY KEY (IdCategoriaMovimento)
GO

CREATE TABLE dbo.Estabelecimento
(
	IdEstabelecimento INT IDENTITY NOT NULL,
	NomeEstabelecimento VARCHAR(100) NOT NULL
)
GO
ALTER TABLE dbo.Estabelecimento ADD CONSTRAINT Pk_Estabelecimento PRIMARY KEY (IdEstabelecimento)
GO

CREATE TABLE dbo.CategoriaEstabelecimento
(
	IdCategoriaMovimento INT NOT NULL,
	IdEstabelecimento INT NOT NULL
)
GO
ALTER TABLE dbo.CategoriaEstabelecimento ADD CONSTRAINT Fk_CategoriaMovimento FOREIGN KEY (IdCategoriaMovimento) REFERENCES dbo.CategoriaMovimento (IdCategoriaMovimento)
GO
ALTER TABLE dbo.CategoriaEstabelecimento ADD CONSTRAINT Fk_Estabelecimento FOREIGN KEY (IdEstabelecimento) REFERENCES dbo.Estabelecimento (IdEstabelecimento)
GO

CREATE TABLE dbo.OrigemMovimento
(
	IdOrigemMovimento INT IDENTITY NOT NULL,
	Descricao VARCHAR(10) NOT NULL
)
GO
ALTER TABLE dbo.OrigemMovimento ADD CONSTRAINT Pk_OrigemMovimento PRIMARY KEY (IdOrigemMovimento)
GO

CREATE TABLE dbo.Movimento
(
	IdMovimento INT IDENTITY NOT NULL,
	IdOrigemMovimento INT NOT NULL,
	IdCategoriaMovimento INT NOT NULL,
	IdConta INT NOT NULL,
	DataInclusao DATETIME DEFAULT GETDATE(),
	Valor DECIMAL(10, 2) NOT NULL,
	QtdParcelas INT NOT NULL,
	DataVencimento DATETIME NULL
)
GO
ALTER TABLE dbo.Movimento ADD CONSTRAINT Pk_Movimento PRIMARY KEY (IdMovimento)
GO
ALTER TABLE dbo.Movimento ADD CONSTRAINT FK_MovimentoOrigemMovimento FOREIGN KEY (IdOrigemMovimento) REFERENCES dbo.OrigemMovimento (IdOrigemMovimento)
GO
ALTER TABLE dbo.Movimento ADD CONSTRAINT FK_MovimentoCategoriaMovimento FOREIGN KEY (IdCategoriaMovimento) REFERENCES dbo.CategoriaMovimento (IdCategoriaMovimento)
GO
ALTER TABLE dbo.Movimento ADD CONSTRAINT FK_MovimentoConta FOREIGN KEY (IdConta) REFERENCES dbo.Conta (IdConta)
GO

--========================================================================================================================

INSERT INTO dbo.TipoConta ( Descricao ) VALUES ( 'Conta Corrente' )
GO
INSERT INTO dbo.TipoConta ( Descricao ) VALUES ( 'Conta Poupança' )
GO
INSERT INTO dbo.Conta ( TitularConta, IdTipoConta ) VALUES ( 'Lucas Beck', 1 )
GO
INSERT INTO dbo.CategoriaMovimento ( Descricao ) VALUES ( 'Alimentação' )
GO
INSERT INTO dbo.Estabelecimento ( NomeEstabelecimento ) VALUES ( 'Panquecas Alemão' )
GO
INSERT INTO dbo.CategoriaEstabelecimento ( IdCategoriaMovimento, IdEstabelecimento ) VALUES ( 1, 1 )
GO
INSERT INTO dbo.OrigemMovimento (Descricao) VALUES ('Débito')
GO
INSERT INTO dbo.OrigemMovimento (Descricao) VALUES ('Crédito')
GO
INSERT INTO dbo.Movimento
(
	IdOrigemMovimento,
	IdCategoriaMovimento,
	IdConta,
	Valor,
	QtdParcelas,
	DataVencimento
)
VALUES
(
	1,
	1,
	1,
	22.00,
	1,
	NULL
)
GO
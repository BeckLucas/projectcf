using System.Collections.Generic;
using System.Data;
using System.Linq;
using ControleFinanceiro.Infra.DbContext;
using Dapper.Contrib.Extensions;

namespace ControleFinanceiro.Infra.Repository
{
    public abstract class AbstractRepository<T> where T: class, new()
    {
        protected readonly IDbConnection _connection;

        public AbstractRepository(DapperDbContext dbContext)
        {
            _connection = dbContext.DbConnection;
        }

        public long Add(T obj)
        {
            return _connection.Insert(obj);
        }

        public T FindById(int id)
        {
            return _connection.Get<T>(id);
        }

        public void Remove(T obj)
        {
            _connection.Delete<T>(obj);
        }

        public void Remove(int id)
        {
            _connection.Delete<T>(FindById(id));
        }

        public void Update(T obj)
        {
            _connection.Update<T>(obj);
        }

        public List<T> FindAll()
        {
            return _connection.GetAll<T>().ToList();
        }
    }
}
using System;
using System.Data;
using System.Data.SqlClient;

namespace ControleFinanceiro.Infra.DbContext
{
    public class DapperDbContext : IDisposable
    {
        public IDbConnection DbConnection { get; private set; }

        public DapperDbContext(string connectionString)
        {
            DbConnection = new SqlConnection(connectionString);
        }

        public void Dispose()
        {
            DbConnection.Close();
        }
    }
}
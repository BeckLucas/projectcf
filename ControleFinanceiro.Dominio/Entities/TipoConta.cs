namespace ControleFinanceiro.Dominio.Entities
{
    public class TipoConta : BaseEntity
    {
        public int IdTipoConta { get; set; }
        public string Descricao { get; set; }
    }
}
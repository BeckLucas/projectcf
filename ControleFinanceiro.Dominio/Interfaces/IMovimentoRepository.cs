using System;
using ControleFinanceiro.Dominio.Entities;

namespace ControleFinanceiro.Dominio.Interfaces
{
    public interface IMovimentoRepository
    {
         void AdicionarMovimento(Movimento movimento);
         void RemoverMovimento(Movimento movimento);
         decimal ObterTotalMovimentacaoDiaria(DateTime dtInicial, DateTime dtFinal);
         DateTime ObterDataVencimentoMaisAtual(string regiao);
         DateTime ObterDataVencimentoPadrao();
    }
}
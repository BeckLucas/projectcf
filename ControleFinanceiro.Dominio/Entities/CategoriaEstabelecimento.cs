namespace ControleFinanceiro.Dominio.Entities
{
    public class CategoriaEstabelecimento : BaseEntity
    {
        public CategoriaMovimento CategoriaMovimento { get; set; }
        public Estabelecimento Estabelecimento { get; set; }
    }
}
using System;
using ControleFinanceiro.Dominio.Interfaces;
using ControleFinanceiro.Dominio.Services;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ControleFinanceiro.Tests
{
    [TestClass]
    public class RepositoryTest
    {
        private Mock<IMovimentoRepository> _mockRepository;
        private MovimentacaoDiariaService _service;
        private Mock<ILogger<MovimentacaoDiariaService>> _mockLogger;
        
        [TestInitialize]
        public void TestInitialize()
        {
            _mockRepository = new Mock<IMovimentoRepository>();
            _mockLogger = new Mock<ILogger<MovimentacaoDiariaService>>();
        }

        [TestMethod]
        public void TestMovimentacao()
        {
            // Arrange
            _mockRepository.Setup(x => x.ObterTotalMovimentacaoDiaria(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(decimal.Parse("20,00"));

            // Act
            _service = new MovimentacaoDiariaService(_mockLogger.Object, _mockRepository.Object);
            var retorno = _service.ObterValorMovimentacaoDiariaEspecial(DateTime.Now, DateTime.Now);

            // Assert
            _mockRepository.Verify(x => x.ObterTotalMovimentacaoDiaria(It.IsAny<DateTime>(), It.IsAny<DateTime>()), Times.Once);
            Assert.AreEqual(retorno, decimal.Parse("20,00"));
        }
    }
}

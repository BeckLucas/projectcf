﻿using System;
using System.Collections.Generic;
using ControleFinanceiro.Dominio.Services;
using Microsoft.AspNetCore.Mvc;

namespace ControleFinanceiro.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimentacaoController : ControllerBase
    {
        private readonly MovimentacaoDiariaService movimentacaoDiariaService;

        public MovimentacaoController(MovimentacaoDiariaService service)
        {
            this.movimentacaoDiariaService = service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var retorno = this.movimentacaoDiariaService.ObterValorMovimentacaoDiariaEspecial(DateTime.Now.AddDays(-1), DateTime.Now);
            
            return new string[] { retorno.ToString() };
        }
    }
}

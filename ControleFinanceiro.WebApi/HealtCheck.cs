using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace ControleFinanceiro.WebApi
{
    public class HealtCheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            HttpClient client = new HttpClient();

            var result = await client.GetAsync("http://demo2904779.mockable.io/test-healt");

            if (result.IsSuccessStatusCode)
            { 
                return HealthCheckResult.Healthy("A API está funcionando corretamente!");
            } 
            else
            {
                return HealthCheckResult.Unhealthy("A API não está funcionando corretamente!");
            }
        }
    }
}
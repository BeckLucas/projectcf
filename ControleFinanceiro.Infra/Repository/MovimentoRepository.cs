using System;
using ControleFinanceiro.Dominio.Entities;
using ControleFinanceiro.Dominio.Interfaces;
using ControleFinanceiro.Infra.DbContext;
using Dapper;

namespace ControleFinanceiro.Infra.Repository
{
    public class MovimentoRepository : AbstractRepository<Movimento>, IMovimentoRepository
    {
        public MovimentoRepository(DapperDbContext dbContext) : base(dbContext) { }

        public void AdicionarMovimento(Movimento movimento)
        {
            var inclusao = base.Add(movimento);
        }

        public void RemoverMovimento(Movimento movimento)
        {
            base.Remove(movimento);
        }

        public virtual decimal ObterTotalMovimentacaoDiaria(DateTime dtInicial, DateTime dtFinal)
        {
            return base._connection.ExecuteScalar<decimal>(@"SELECT	Total
                                                             FROM   dbo.Conta");
        }

        public DateTime ObterDataVencimentoMaisAtual(string regiao)
        {
            return base._connection.ExecuteScalar<DateTime>(@"SELECT  MAX(DataVencimento)
                                                             FROM     dbo.Regioes
                                                             WHERE    Regiao = @Regiao", new { Regiao = regiao });
        }

        public DateTime ObterDataVencimentoPadrao()
        {
            return base._connection.ExecuteScalar<DateTime>(@"SELECT  DataMovimento
                                                             FROM     dbo.Sistema ");
        }
    }
}
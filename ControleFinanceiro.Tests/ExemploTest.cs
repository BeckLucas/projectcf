using System;
using ControleFinanceiro.Dominio.Interfaces;
using ControleFinanceiro.Dominio.Services;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ControleFinanceiro.Tests
{
    [TestClass]
    public class ExemploTest
    {
        private MovimentacaoDiariaService movimentacaoDiariaService;
        private Mock<IMovimentoRepository> mockRepository;
        private Mock<ILogger<MovimentacaoDiariaService>> mockLogger;

        [TestInitialize]
        public void TestInitialize()
        {
            mockRepository = new Mock<IMovimentoRepository>();
            mockLogger = new Mock<ILogger<MovimentacaoDiariaService>>();
        }

        [TestMethod]
        public void Test_ObterDataVencimentoAtualRegiao()
        {
            // Arrange
            var retornoRepo = DateTime.Now.AddDays(-1);

            mockRepository.Setup(x => x.ObterDataVencimentoMaisAtual(It.IsAny<string>()))
                .Returns(retornoRepo);
            
            // Act
            movimentacaoDiariaService = new MovimentacaoDiariaService(mockLogger.Object, mockRepository.Object);
            var dataVencimentoAtual = movimentacaoDiariaService.ObterProximaDataVencimento("RS");

            // Assert
            mockRepository.Verify(x => x.ObterDataVencimentoMaisAtual(It.IsAny<string>()), Times.Once);
            Assert.AreEqual(dataVencimentoAtual, retornoRepo);
        }

        [TestMethod]
        public void Test_ObterDataVencimentoPadrao()
        {
            // Arrange
            mockRepository.Setup(x => x.ObterDataVencimentoMaisAtual(It.IsAny<string>()))
                .Returns(DateTime.MinValue);

            var retornoRepo = DateTime.Now;

            mockRepository.Setup(x => x.ObterDataVencimentoPadrao())
                .Returns(retornoRepo);

            // Act
            movimentacaoDiariaService = new MovimentacaoDiariaService(null, mockRepository.Object);
            var dataVencimentoAtual = movimentacaoDiariaService.ObterProximaDataVencimento("RS");

            // Assert
            Assert.AreEqual(dataVencimentoAtual, retornoRepo);
        }
    }
}
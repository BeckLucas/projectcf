namespace ControleFinanceiro.Dominio.Entities
{
    public class Estabelecimento : BaseEntity
    {
        public int IdEstabelecimento { get; set; }
        public string NomeEstabelecimento { get; set; }
    }
}
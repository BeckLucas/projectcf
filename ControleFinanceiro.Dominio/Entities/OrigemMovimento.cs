namespace ControleFinanceiro.Dominio.Entities
{
    public class OrigemMovimento : BaseEntity
    {
        public int IdOrigemMovimento { get; set; }
        public string Descricao { get; set; }
    }
}
using System;

namespace ControleFinanceiro.Dominio.Entities
{
    public class CategoriaMovimento : BaseEntity
    {
        public int IdCategoriaMovimento { get; set; }
        public string Descricao { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}
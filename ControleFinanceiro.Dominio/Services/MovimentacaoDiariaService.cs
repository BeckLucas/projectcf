using System;
using ControleFinanceiro.Dominio.Interfaces;
using Microsoft.Extensions.Logging;

namespace ControleFinanceiro.Dominio.Services
{
    public class MovimentacaoDiariaService
    {
        private readonly ILogger<MovimentacaoDiariaService> logger;
        private readonly IMovimentoRepository movimentoRepository;

        public MovimentacaoDiariaService(ILogger<MovimentacaoDiariaService> logger, IMovimentoRepository repository)
        {
            this.logger = logger;
            this.movimentoRepository = repository;
        }

        public decimal ObterValorMovimentacaoDiariaEspecial(DateTime dtInicial, DateTime dtFinal)
        {
            logger.LogWarning("Realizada requisição a API");

            var retorno = this.movimentoRepository.ObterTotalMovimentacaoDiaria(dtInicial, dtFinal);

            return retorno;
        }

        public DateTime ObterProximaDataVencimento(string regiao)
        {
            if(string.IsNullOrEmpty(regiao))
            {
                throw new ArgumentException("A região deve ser informada.");
            }

            var dataVencimentoAtual = this.movimentoRepository.ObterDataVencimentoMaisAtual(regiao);

            if(dataVencimentoAtual != null && dataVencimentoAtual != DateTime.MinValue)
            {
                return dataVencimentoAtual;
            } else {
                var dataVencimentoPadrao = this.movimentoRepository.ObterDataVencimentoPadrao();

                return dataVencimentoPadrao;
            }
        }
    }
}
using ControleFinanceiro.Dominio.Interfaces;
using ControleFinanceiro.Dominio.Services;
using ControleFinanceiro.Infra.DbContext;
using ControleFinanceiro.Infra.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ControleFinanceiro.WebApi.Config
{
    public class DependencyInjection
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddScoped(_ => new DapperDbContext(configuration.GetSection("SqlConnection:ConnectionString").Value))
                .AddScoped<IMovimentoRepository, MovimentoRepository>();

            services
                .AddScoped<MovimentacaoDiariaService>();
        }
    }
}
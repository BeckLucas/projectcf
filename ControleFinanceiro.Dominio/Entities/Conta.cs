namespace ControleFinanceiro.Dominio.Entities
{
    public class Conta : BaseEntity
    {
        public int IdConta { get; set; }
        public string TitularConta { get; set; }
        public TipoConta TipoConta { get; set; }
    }
}